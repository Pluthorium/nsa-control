import psutil
import time
from datetime import datetime
#Zuständig für E-Mail versendungen
import smtplib
#Zuständig für unseren Unittest
import unittest
from email.message import EmailMessage
#Zuständig für Config.ini
from configparser import ConfigParser


#Die Variable der Zeit wird definiert und in einem lesbaren format umgewandelt
dateTimeObj = datetime.now()
timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S)")

#Variablen werden hier Definiert
MemoryCount = 0
CPUCount = 0
DiskCount = 0
config = ConfigParser()
CPUUsage = psutil.cpu_percent(interval=None, percpu=False)
MemoryUsage = psutil.virtual_memory().percent
DiskUsage = psutil.disk_usage('C:').percent

#Variablen für die Abfragen
option = ""
abfrage = ""
HelpAbfragen = ""
MehrAbfragen = "Y"
DiskPlatz = psutil.disk_usage("C:")

#Dateierstellung zur Aufzeichnung der Benutzer Daten und Warnungen
Logfile = open('Logfile.txt','a')
Benutzer = open('Benutzer.txt','a')
config.read('config.ini')

str_Benutzer = config.get("CONFIG", "Benutzer" )
str_Passwort = config.get("CONFIG", "Passwort" )

int_CPUUsage = config.getint("CONFIG", "CPUUsage")
int_CPUWarnLimit = config.getint("CONFIG","CPUWarnLimit")
int_MemoryUsage = config.getint("CONFIG", "MemoryUsage")
int_MemoryWarnLimit = config.getint("CONFIG", "MemoryWarnLimit")
int_DiskUsage = config.getint("CONFIG", "DiskUsage")
int_DiskWarnLimit = config.getint("CONFIG", "DiskWarnLimit")

#Login
Admin = 0
Name = input("Bitte Benutzername eingeben:")
Benutzer.write("\n" + str(timestampStr) + str(Name))

if Name == str_Benutzer:
    pw = input("Wenn sie sich als Admin anmelden möchten bitte passwort eingeben, wenn nicht Enter drücken: ")
    if pw == str_Passwort:
        Admin = 1
        print()
        print("### Sie sind als Admin angemeldet ###")
    else:
        Admin = 0
        print()
        print("!!! Das eingegebende Passwort ist falsch !!!")
        print()
        print("### Sie sind als User angemeldet ###")

print()
#Abfragen beim start
Laufzeit = psutil.boot_time()
LaufzeitUmgewandelt = datetime.fromtimestamp(Laufzeit)

if Admin == 1:
    option = str(input("Willst du in die optionen Y/N: "))
    if option == "Y" or option == "y":
        abfrage = str(input("Gib den Befehl Help ein um mehr zu wissen oder Enter für weiter: "))
        if abfrage == "Help" or abfrage == "help":
            print("Die Befehle lauten: CPU, Disk, Memory, Laufzeit")
            while MehrAbfragen == "Y" or MehrAbfragen == "y":
                HelpAbfragen = str(input("Eingabe: "))
                if HelpAbfragen == "CPU" or HelpAbfragen == "cpu":
                    print("Aktuelle CPU auslastung bei: " + str(psutil.cpu_percent(interval=None, percpu=False)) + "%")
                elif HelpAbfragen == "Disk" or HelpAbfragen == "disk":
                    print("Aktueller Speicherauslastung der Disk bei: " + str(DiskPlatz.percent) + "%")
                elif HelpAbfragen == "Memory" or HelpAbfragen == "memory":
                    print("Aktuelle Memory auslastung bei: " + str(psutil.virtual_memory().percent) + "%")
                elif HelpAbfragen == "Laufzeit" or HelpAbfragen == "laufzeit":
                    print(
                        f"Das Gerät ist in Betrieb seit dem: {LaufzeitUmgewandelt.year}/{LaufzeitUmgewandelt.month}/{LaufzeitUmgewandelt.day} {LaufzeitUmgewandelt.hour}:{LaufzeitUmgewandelt.minute}:{LaufzeitUmgewandelt.second}")
                MehrAbfragen = str(input("Möchtest du noch mehr abfragen machen? Y/N: "))

        abfrage = str(input("Gib den Befehl Help ein um mehr zu wissen oder Enter für weiter: "))

if Admin == 0:
    option = str(input("Willst du in die optionen Y/N: "))
    if option == "Y" or option == "y":
        abfrage = str(input("Gib den Befehl Help ein um mehr zu wissen oder Enter für weiter: "))
        if abfrage == "Help" or abfrage == "help":
            print("Die Befehle lauten: CPU, Disk, Memory")
            while MehrAbfragen == "Y" or MehrAbfragen == "y":
                HelpAbfragen = str(input("Eingabe: "))
                if HelpAbfragen == "CPU" or HelpAbfragen == "cpu":
                    print("Aktuelle CPU auslastung bei: " + str(psutil.cpu_percent(interval=None, percpu=False)) + "%")
                elif HelpAbfragen == "Disk" or HelpAbfragen == "disk":
                    print("Aktueller Speicherauslastung der Disk bei: " + str(DiskPlatz.percent) + "%")
                elif HelpAbfragen == "Memory" or HelpAbfragen == "memory":
                    print("Aktuelle Memory auslastung bei: " + str(psutil.virtual_memory().percent) + "%")
                MehrAbfragen = str(input("Möchtest du noch mehr abfragen machen? Y/N: "))
        print()
        print("Es werden nun Systemauslastungen angezeigt, wenn diese vorhanden sind.")

else:
    print("Es werden nun Systemauslastungen angezeigt, wenn diese vorhanden sind.")
#Email funktion
def Email():
    msg = EmailMessage()
    if MemoryCount > 2:
        msg.set_content("Die Memory auslastung ist bei" + str(psutil.virtual_memory().percent) + "% und der Schwellwert wurde mehrfach überschritten")
    elif CPUCount > 2:
        msg.set_content("Die CPU auslastung ist bei" + str(psutil.cpu_percent(interval=None, percpu=False)) + "% und der Schwellwert wurde mehrfach überschritten")
    elif DiskCount > 2:
        msg.set_content("Die Disk auslastung ist bei" + str(psutil.disk_usage.percent) + "% und der Schwellwert wurde mehrfach überschritten")
    msg['Subject'] = 'Server Warnung'
    msg['From'] = "Systemparameterupdate@gmail.com"
    msg['To'] = "MartinSchulz424@gmail.com"

    try:
        #Sende Nachricht via SMTP
        print()
        print("Es wird versucht sich mit dem Email server zu verbinden")
        print()
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.login("systemparameterupdate@gmail.com", "systemparameter") #Login-Email und Login-Pw
        server.send_message(msg)
        server.quit()
    except:
        print()
        print("Die Memory Email konnte nicht versendet werden da die Ports nicht freigeschaltet wurden")

#Funktion um die CPU auslastung auszulesen und auszuwerten
def CPU(CPUUsage):
    global CPUCount
    #CPU Usage in %
    if CPUUsage < 0 or CPUUsage > 100:
        print("Es besteht wohlmöglich ein Fehler mit dem Bauteil")
        return False
    elif CPUUsage >= int_CPUUsage:
        CPUCount = CPUCount +1
        print(timestampStr + ": CPU usage over "+ str(CPUUsage) +"%")
    else:
        CPUCount = 0
        return True
    if CPUCount > 2:
        print(timestampStr + ": CPU Warning")
        Logfile.write("\n" + str(timestampStr) + "Warning! CPU usage at " + str(CPUUsage) + "%")
        if CPUCount == int_CPUWarnLimit:
            Email()

#Funktion um die Memory auslastung auszulesen und auszuwerten
def Memory(MemoryUsage):
    global MemoryCount
    # RAM Usage in %
    if MemoryUsage < 0 or MemoryUsage > 100:
        print("Es besteht wohlmöglich ein Fehler mit dem Bauteil")
        return False
    elif MemoryUsage >= int_MemoryUsage:
        MemoryCount = MemoryCount + 1
        print(timestampStr + ": Memory Usage over "+ str(MemoryUsage) +"%")
    else:
        MemoryCount = 0
        return True
    if MemoryCount > 2:
        print(timestampStr + ": Memory Warning")
        Logfile.write("\n" + str(timestampStr) + "Warning! Memory usage at " + str(MemoryUsage) + "%")
        if MemoryCount == int_MemoryWarnLimit:
            Email()
#Funktion um die Auslastung des Massenspeichers auszulesen und auszuwerten
def Disk(DiskUsage):
    global DiskCount
    # Disk Usage in %
    if DiskUsage < 0 or DiskUsage > 100:
        print("Es besteht wohlmöglich ein Fehler mit dem Bauteil")
        return False
    elif DiskUsage >= int_DiskUsage:
        DiskCount = DiskCount + 1
        print(timestampStr, ": C:\ Diskspace over "+ str(DiskUsage) +"%")
    else:
        DiskCount = 0
        return True
    if DiskCount > 2:
        print(timestampStr + ":C:\ Disk Warning")
        Logfile.write("\n" + str(timestampStr) + "Warning! Disk usage at " + str(DiskUsage.percent) + "%")
        if DiskCount == int_DiskWarnLimit:
            Email()
"""""

#Hier drunter sind drei Unitstest, bei Testberaf den Multiline Comment entfernen

class test_NSATool(unittest.TestCase):
    def test_CPU(self):
        self.assertFalse(CPU(-1))
        self.assertTrue(CPU(10))
        self.assertTrue(CPU(50))
        self.assertFalse(CPU(101))
    def test_Memory(self):
        self.assertFalse(Memory(-1))
        self.assertTrue(Memory(10))
        self.assertTrue(Memory(50))
        self.assertFalse(Memory(101))
    def test_Disk(self):
        self.assertFalse(Disk(-1))
        self.assertTrue(Disk(10))
        self.assertTrue(Disk(50))
        self.assertFalse(Disk(101))

if __name__ == "__main__":
    unittest.main()
"""""

#Eine Dauerhatfe schleife damit das Monitoring 24/7 im bestenfall laufen kann, es sei denn eine Hardware hat ein Fehler
while True:
    CPU(CPUUsage)
    Memory(MemoryUsage)
    Disk(DiskUsage)
    time.sleep(2)



